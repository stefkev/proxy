var http = require('http');
var httpProxy = require('http-proxy');
var express = require('express');
var _ = require('lodash');
var request = require("request");
var app = express();

// CONSTANTS
var W2_PROXY_LOCATION = 'localhost:8000',
    T2_PROXY_LOCATION = 'localhost:8001';

/* We use harmon to change the response before returning the content to the user.
 * Below are the rules
 */

// Prevent the page from changing the base url
// specifically, on http://w2.leisurelink.lcsd.gov.hk//leisurelink/humanTest/humanTest.jsp and later on the login page
// there are things like <base href="http://w2.leisurelink.lcsd.gov.hk/leisurelink/humanTest/humanTest.jsp">
// Which would make subsequent request go directly to http://w2.leisurelink.lcsd.gov.hk
// Let's make sure they don't screw the proxy
var removeBaseModification = {};
removeBaseModification.query = 'base';
removeBaseModification.func = function (node) {
    node.getAttribute('href', function(attr) {
        var w2Url = 'http://w2.leisurelink.lcsd.gov.hk';
        var t2Url = 'https://t2.leisurelink.lcsd.gov.hk';
        if(attr.indexOf(w2Url) > -1){
            node.setAttribute('href', 'http://' + W2_PROXY_LOCATION + attr.substring(w2Url.length));
        }else if(attr.indexOf(t2Url) > -1){
            node.setAttribute('href', 'http://' + T2_PROXY_LOCATION + attr.substring(t2Url.length));
        }
       });
};

// Once we pass the captcha, there is a form that redirect us to 'https://t2.leisurelink.lcsd.gov.hk'
// Change it to our proxy.
var t2Replace = {};
t2Replace.query = 'form#forwardForm';
t2Replace.func = function (node) {
    node.getAttribute('action', function(attr) {
        var prefix = 'https://t2.leisurelink.lcsd.gov.hk';
        node.setAttribute('action', 'http://' + T2_PROXY_LOCATION + attr.substring(prefix.length));
    });
}

// There is a javascript check to see if the page is opened in a new window (checkParentBrowser)
// Let's get rid of that check by making that function a no-op
var replaceCheckParentBrowser = {};
replaceCheckParentBrowser.query = 'script';
replaceCheckParentBrowser.func = function (node) {
    node.getAttribute('src', function(attr) {
        if(attr === undefined){
            // This will add the snippet on every single script tag that is not an import.
            // Wasteful, to fix later
            // Get the script content as a stream
            var stm = node.createStream();
            stm.on('data', function(data) {
                // Simply write it again, we won't be modifying the content.
                stm.write(data);
            });
            // When the read side of the stream has ended:
            stm.on('end', function() {
              // Now on the write side of the stream write some data using .end()
              // N.B. if end isn't called it will just hang.
              stm.end('\nfunction checkParentBrowser(){}\n');
            });
            stm.on('error',function(err){
                console.log(err);
            })
        }
    });
}

/* Map w2 to our proxy */
var w2App = express();
var w2Proxy = httpProxy.createProxyServer({
    target: 'http://w2.leisurelink.lcsd.gov.hk',
    autoRewrite: true,
    changeOrigin: true
});
w2App.use(require('harmon')([], [removeBaseModification, t2Replace]));
w2App.use(function (req, res) {
   w2Proxy.web(req, res);
});
http.createServer(w2App).listen(8000);
console.log("w2 listening on port 8000");

/* Map t2 to our proxy */
var t2Proxy = httpProxy.createProxyServer({
    target: 'https://t2.leisurelink.lcsd.gov.hk',
    autoRewrite:true,
    changeOrigin: true,
    secure: false
});
w2Proxy.on('error',function(err){
    console.log(err);
})
// HTTPS cookies have a 'Secure' flag. We are stripping the HTTPS so the flag must be removed.
// Otherwise the browser will ignore that cookie.
t2Proxy.on('proxyRes', function (proxyRes, req, res) {
  proxyRes.headers["set-cookie"] = _.map(proxyRes.headers["set-cookie"], function(c){
    return c.replace(";Secure");
  });
});
t2Proxy.on('error', function(err){
    console.log(err);
})

var t2App = express();
t2App.use(require('harmon')([], [removeBaseModification, replaceCheckParentBrowser]));
t2App.use(function (req, res) {
           t2Proxy.web(req, res);
        });
http.createServer(t2App).listen(8001);
console.log("t2 on port 8001");
app.listen(1000);
console.log('server on 1000')



var cookie = request.jar();
var requests = request.defaults({jar:cookie});
var reqbody;
var reqhead;

requests.get("http://localhost:8001", function(err, resp, body){
    console.log("finished init load");
    myreq = requests.get("http://localhost:8000/leisurelink/application/checkCode.do?flowId=2&lang=EN", function(err, resp, body){
        console.log('finished check')
        reqbody += body;
        reqhead = resp.headers;
    })

});
app.get('/',function(req, res){
    console.log(reqbody);
    
   res.body = reqbody;
    res.headers = reqhead;
    res.send();
})